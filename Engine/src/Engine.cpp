#include <Engine.h>

using namespace ENGINE;
using namespace ENGINE::CORE;

Engine::Engine()
{

}

//You can set params for init
bool Engine::Init(int argc, char* argv[], WindowInfo window, ContextInfo context, FrameBufferInfo frameBufferInfo)
{
   INIT::Init_GLUT::init(argc, argv, window, context, frameBufferInfo);
 
   m_scene_manager = new MANAGERS::SceneManager();
 
   INIT::Init_GLUT::setListener(m_scene_manager);
 
   //this was created in  scene manager constructor, now copy here
   m_material_manager = new MANAGERS::MaterialManager();
   m_material_manager->Init();
   
   if (m_scene_manager && m_material_manager)
   {
      m_object_manager = new MANAGERS::ObjectManager();
      m_scene_manager->SetObjectManager(m_object_manager);
   }
   else
   {
      return false;
   }
 
   return true;
}

bool Engine::Init(int argc, char* argv[])
{
   WindowInfo window("test", 2560, 1080, 0, 0, true);
   ContextInfo context(3, 0);
   FrameBufferInfo frameBufferInfo(true, true, true, true);

   return Engine::Init(argc, argv, window, context, frameBufferInfo);
}
 
//Create the loop
void Engine::Run()
{
   INIT::Init_GLUT::run();
}
 
MANAGERS::SceneManager* Engine::GetScene_Manager() const
{
   return m_scene_manager;
}
 
MANAGERS::MaterialManager* Engine::GetMaterial_Manager() const
{
   return m_material_manager;
}
 
MANAGERS::ObjectManager* Engine::GetObject_Manager() const
{
   return m_object_manager;
}
 
Engine::~Engine()
{
  if (m_scene_manager)
      delete m_scene_manager;
 
   if (m_material_manager)
      delete m_material_manager;
 
   if (m_object_manager)
      delete m_object_manager;
}