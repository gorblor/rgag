#include <Managers/MaterialManager.h>

using namespace ENGINE::MANAGERS;
using namespace ENGINE::RENDERING;

MaterialManager::MaterialManager(void)
{
    
}

MaterialManager::~MaterialManager(void)
{
    if (m_shader_manager)
		delete m_shader_manager;
    if (m_texture_loader)
		delete m_texture_loader;
}

void MaterialManager::Init()
{
    m_shader_manager = new MANAGERS::ShaderManager();
    m_shader_manager->CreateProgram("MaterialShader", "../Shaders/tex_VertexShader.glsl", "../Shaders/tex_FragmentShader.glsl");

    m_texture_loader = new TextureLoader();
}

void MaterialManager::CreateMaterial(std::string materialName, std::vector<Texture> textures)
{
    Material* material = new Material();
    material->SetProgram(m_shader_manager->GetShader("MaterialShader"));
    for(auto texture : textures)
    {
        material->SetTexture(texture.tex_name, m_texture_loader->LoadTexture(texture.tex_path, texture.width, texture.height));
    }
    materials[materialName] = *material;
}

void MaterialManager::CreateMaterial(std::string materialName, Texture texture)
{
    Material* material = new Material();
    material->SetProgram(m_shader_manager->GetShader("MaterialShader"));
    material->SetTexture(texture.tex_name, m_texture_loader->LoadTexture(texture.tex_path, texture.width, texture.height));
    materials[materialName] = *material;
}

const Material MaterialManager::GetMaterial(std::string materialName)
{
	return materials.at(materialName);
}

TextureLoader* MaterialManager::GetTexture_Loader() const
{
	return m_texture_loader;
}