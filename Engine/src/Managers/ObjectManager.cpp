#include <Managers/ObjectManager.h>

using namespace ENGINE::MANAGERS;
using namespace ENGINE::RENDERING;

ObjectManager::ObjectManager()
{
 
}
 
ObjectManager::~ObjectManager()
{
  //auto -it's a map iterator
  for (auto object: gameObjectList)
  {
    delete object.second;
  }
  gameObjectList.clear();

  for (auto object : gameObjectList_NDC)
	{
		delete object.second;
	}
	gameObjectList_NDC.clear();
}

void ObjectManager::Update()
{
   //auto -it's a map iterator
  for (auto object: gameObjectList)
  {
    object.second->Update();
  }
  for (auto object : gameObjectList_NDC)
	{
		object.second->Update();
	}
}

void ObjectManager::Draw()
{
   //auto -it's a map iterator
   for (auto object : gameObjectList_NDC)
   {
     object.second->Draw();
   }
}

void ObjectManager::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix)
{
	for (auto object : gameObjectList)
	{
		object.second->Draw(projection_matrix, view_matrix);
	}
}
 
void ObjectManager::DeleteObject(const std::string& gameObjectName)
{
  GameObject* object = gameObjectList[gameObjectName];
  object->Destroy();
  gameObjectList.erase(gameObjectName);
}

void ObjectManager::DeleteObject_NDC(const std::string& gameObjectName)
{
	GameObject* object = gameObjectList_NDC[gameObjectName];
	object->Destroy();
	gameObjectList_NDC.erase(gameObjectName);
}
 
const GameObject& ObjectManager::GetObject(const std::string& gameObjectName) const
{
  return (*gameObjectList.at(gameObjectName));
}

const GameObject& ObjectManager::GetObject_NDC(const std::string& gameObjectName) const
{
	return (*gameObjectList_NDC.at(gameObjectName));
}

void ObjectManager::SetObject(const std::string& gameObjectName, RENDERING::GameObject* gameObject)
{
	gameObjectList[gameObjectName.c_str()] = gameObject;
}