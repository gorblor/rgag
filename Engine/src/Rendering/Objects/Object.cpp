#include "Rendering/Objects/Object.h"

using namespace ENGINE::RENDERING;
using namespace ENGINE::RENDERING::OBJECTS;
 
Object::Object(){}
 
Object::~Object()
{
   Destroy();
}
 
void Object::Draw()
{
   //this will be again overridden
}

void Object::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix)
{
   //this will be again overridden
}
 
void Object::Update()
{
 //this will be again overridden
}

GLuint Object::GetVao() const
{
  return vao;
}
 
const std::vector<GLuint>& Object::GetVbos() const
{
   return vbos;
}

const Material Object::GetMaterial() const
{
	return material;
}


void Object::SetMaterial(Material material)
{
	this->material = material;
}
 
void Object::Destroy()
{
   glDeleteVertexArrays(1, &vao);
   glDeleteBuffers(vbos.size(), &vbos[0]);
   vbos.clear();
   //glDeleteTextures(1, &material.GetTexture(""));
}