#include "Rendering/Light/Light.h"

using namespace ENGINE::RENDERING;
using namespace ENGINE::RENDERING::LIGHT;

Light::Light(){}
 
Light::~Light()
{
   Destroy();
}
 
void Light::Draw()
{
   //this will be again overridden
}

void Light::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix)
{
   //this will be again overridden
}
 
void Light::Update()
{
 //this will be again overridden
}

GLuint Light::GetVao() const
{
  return vao;
}
 
const std::vector<GLuint>& Light::GetVbos() const
{
   return vbos;
}

/*const Material Light::GetMaterial() const
{
	return material;
}


void Light::SetMaterial(Material material)
{
	this->material = material;
}*/
 
void Light::Destroy()
{
   glDeleteVertexArrays(1, &vao);
   glDeleteBuffers(vbos.size(), &vbos[0]);
   vbos.clear();
   //glDeleteTextures(1, &material.GetTexture(""));
}