#include <Rendering/Texture/Material.h>
#include <iostream>

using namespace ENGINE::RENDERING;

Material::Material()
{

}

Material::~Material()
{

}

void Material::SetTexture(std::string textureName, GLuint texture)
{
    if (texture == 0) return;
	textures[textureName] = texture;
}

GLuint Material::GetTexture(std::string textureName)
{
    return textures.at(textureName);
}

void Material::SetProgram(GLuint shaderName)
{
    if(shaderName == 0) return;
    this->program = shaderName;
}

GLuint Material::GetProgram()
{
    return program;
}