#include <Rendering/FPSCamera.h>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <GL/glew.h>
#include <GL/freeglut.h>

using namespace ENGINE::RENDERING::CAMERA;

FPSCamera::FPSCamera()
{

}

FPSCamera::~FPSCamera()
{

}

void FPSCamera::UpdateView()
{
 
 //FPS camera:  RotationX(pitch) * RotationY(yaw)
  glm::quat qPitch = glm::angleAxis(pitch, glm::vec3(1, 0, 0));
  glm::quat qYaw = glm::angleAxis(yaw, glm::vec3(0, 1, 0));
  glm::quat qRoll = glm::angleAxis(roll,glm::vec3(0,0,1));
 
 //For a FPS camera we can omit roll
  glm::quat orientation = qPitch * qYaw;
  orientation = glm::normalize(orientation);
  glm::mat4 rotate = glm::mat4_cast(orientation);
 
  glm::mat4 translate = glm::mat4(1.0f);
  translate = glm::translate(translate, -eye);
 
 viewMatrix = rotate * translate;
}

glm::mat4 FPSCamera::GetViewMatrix() const
{
    return viewMatrix;
}

glm::vec3 FPSCamera::GetEyePosition() const
{
    return eye;
}

void FPSCamera::KeyPressed(const unsigned char key, int x, int y)
{
    float dx = 0; //how much we strafe on x
    float dz = 0; //how much we walk on z
    switch (key)
    {
        case 'w':
            dz = 2;
            break;
        case 's':
            dz = -2;
            break;
        case 'a':
            dx = -2;
            break;
        case 'd':
            dx = 2;
            break;
        default:
            break;
    }
   
    //get current view matrix
    glm::mat4 mat = GetViewMatrix();
    //row major
    glm::vec3 forward(mat[0][2], mat[1][2], mat[2][2]);
    glm::vec3 strafe(mat[0][0], mat[1][0], mat[2][0]);
    
    const float speed = 0.12f;//how fast we move
    
    //forward vector must be negative to look forward. 
    //read :<a class="vglnk" href="http://in2gpu.com/2015/05/17/view-matrix/" rel="nofollow"><span>http</span><span>://</span><span>in2gpu</span><span>.</span><span>com</span><span>/</span><span>2015</span><span>/</span><span>05</span><span>/</span><span>17</span><span>/</span><span>view</span><span>-</span><span>matrix</span><span>/</span></a>
    eye += (-dz * forward + dx * strafe) * speed;
        
    //update the view matrix
    UpdateView();
}

void FPSCamera::MouseMove(int x, int y)
{
    if (isMousePressed == false)
        return;
    //always compute delta
    //mousePosition is the last mouse position
    glm::vec2 mouse_delta = glm::vec2(x, y) - mousePosition;
    
    const float mouseX_Sensitivity = 0.25f;
    const float mouseY_Sensitivity = 0.25f;
    //note that yaw and pitch must be converted to radians.
    //this is done in UpdateView() by glm::rotate
    yaw   += mouseX_Sensitivity * mouse_delta.x;
    pitch += mouseY_Sensitivity * mouse_delta.y;
    
    mousePosition = glm::vec2(x, y);
    UpdateView();
}

void FPSCamera::MousePressed(int button, int state, int x, int y)
{
    if (state == GLUT_UP)
    {
        isMousePressed = false;
    }
    if (state == GLUT_DOWN)
    {
        isMousePressed = true;
        mousePosition.x = x;
        mousePosition.y = y;
    }
}