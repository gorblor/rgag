#include <Core/EngineInit.h>

using namespace ENGINE::CORE::INIT;

bool Init_GLEW::Init()
{
    //ENGINE::INIT::Init_GLEW::glewExperimental = true;

    if (glewInit() == GLEW_OK)
	{
		std::cout << "GLEW: Initialized" << std::endl;
	}
	int s = glGetError();
	if (glewIsSupported("GL_VERSION_4_5"))
	{
		std::cout << "GLEW GL_VERSION_4_5 is 4.5\n ";
	}
	else
	{
		std::cout << " GLEW GL_VERSION_4_5 not supported\n ";
	}
    return true;
}