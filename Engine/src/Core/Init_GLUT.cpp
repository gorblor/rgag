#include <Core/EngineInit.h>

using namespace ENGINE;
using namespace ENGINE::CORE;
using namespace ENGINE::CORE::INIT;

EngineListenger* Init_GLUT::listener = NULL;
WindowInfo Init_GLUT::windowInformation;

void Init_GLUT::init(int argc, char* argv[], const WindowInfo& windowInfo, const ContextInfo& contextInfo, const FrameBufferInfo& framebufferInfo)
{
    glutInit(&argc, argv);

    if (contextInfo.core)
    {
        //glut initialization using core profile
        glutInitContextVersion(contextInfo.version_major, contextInfo.version_minor);
        glutInitContextProfile(GLUT_CORE_PROFILE);
    }
    else
    {
        //glut initialization using compability profile
       glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
    }

    //adjusting glut window
    glutInitDisplayMode(framebufferInfo.flags);
    glutInitWindowPosition(windowInfo.position_x, windowInfo.position_y);
    glutInitWindowSize(windowInfo.width, windowInfo.height);
 
    glutInitContextFlags(GLUT_DEBUG);
    glEnable(GL_DEBUG_OUTPUT);
    glutCreateWindow(windowInfo.w_name.c_str());
    std::cout << "GLUT successfully initialized.\n";
    //glEnable(GL_DEBUG_OUTPUT);

    //adding callback functions for communicate with code
    glutIdleFunc(idleCallback);
    glutCloseFunc(closeCallback);
    glutDisplayFunc(displayCallback);
    glutReshapeFunc(reshapeCallback);

    Init_GLEW::Init();

    if (GL_DEBUG_OUTPUT)
	{
		std::cout << "Debug Output supported" << std::endl;
	}

    //these methods require some glew functionality, so we have to
    //move Core::Init::Init_GLEW::Init(); just before calling thse methods
    glDebugMessageCallback(UTIL::EngineDebug::debugCallback, NULL);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);

    //cleanup
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
    OpenGLInfo(windowInfo, contextInfo);
}

void Init_GLUT::run()
{
    glutMainLoop();
}

void Init_GLUT::stop()
{
    glutLeaveMainLoop();
}

void Init_GLUT::idleCallback(void)
{
   //do nothing, just redisplay
   glutPostRedisplay();
}

void Init_GLUT::displayCallback()
{
    if (listener)
    {
        listener->notifyBeginFrame();
        listener->notifyDisplayFrame();
        glutSwapBuffers();
        listener->notifyEndFrame();
    }
}
 
void Init_GLUT::reshapeCallback(int width, int height)
{
    if (windowInformation.isReshapable == true)
    {
        if (listener)
        {
            listener->notifyReshape(width, height, windowInformation.width, windowInformation.height);
        }
        windowInformation.width = width;
        windowInformation.height = height;
    }
}
 
void Init_GLUT::closeCallback()
{
  stop();
}

void Init_GLUT::setListener(MANAGERS::SceneManager*& iListener)
{
    listener = iListener;
}
 
void Init_GLUT::Fullscreen()
{
    if(true)
    {
        glutFullScreen();
    }
    else
    {
        glutLeaveFullScreen();
    }
}

void Init_GLUT::OpenGLInfo(const WindowInfo& windowInfo, const ContextInfo& contextInfo)
{
    const unsigned char* renderer = glGetString(GL_RENDERER);
    const unsigned char* vendor = glGetString(GL_VENDOR);
    const unsigned char* version = glGetString(GL_VERSION);
    
    std::cout << "GLUT:Initialise\n";
    std::cout << "GLUT:\tVendor : " << vendor << std::endl;
    std::cout << "GLUT:\tRenderer : " << renderer << std::endl;
    std::cout << "GLUT:\tOpenGl version: " << version << std::endl;
}