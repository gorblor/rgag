#include <Engine.h>
#include <Rendering/Objects/Cube.h>

#include <Configuration.h>                  //will be generated in cmake

#include <stdio.h>
#include <stdlib.h>

using namespace ENGINE;
using namespace CORE;
using namespace INIT;
using namespace RENDERING;

int main(int argc, char* argv[])
{
    Engine* engine = new Engine();
    engine->Init(argc, argv);
 
    Texture test_tex;
    test_tex.width = 256;
    test_tex.height = 256;
    test_tex.tex_name = "diffuse";
    test_tex.tex_path = "../textures/Crate.bmp";

    //local shaders
    engine->GetMaterial_Manager()->CreateMaterial("Create", test_tex);
    
    OBJECTS::Cube* cube = new OBJECTS::Cube(glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(1, 1, 1));
    cube->SetMaterial(engine->GetMaterial_Manager()->GetMaterial("Create"));
    cube->Create();

    //cube->SetTexture("Create", SOIL_load_OGL_texture("../textures/obamium.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
    
    engine->GetObject_Manager()->SetObject("cube", cube);
    
    engine->Run();

    delete engine;
    return 0;
}