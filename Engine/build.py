import argparse, sys, shutil, os, platform, subprocess

opsystem = platform.system()
project_path = os.path.dirname(os.path.abspath(__file__))
builddir_name = "build"
builddir_path = project_path + "/" + builddir_name
project_name = "rgag"

#cmake - run cmake
#make - run make
#run - run program
#clear

def isPathExists(path):
    return os.path.exists(path)

def getCurrentDirname():
    return os.path.dirname(os.path.realpath(__file__)).split("/")[-1]

def clear(args=""):
    shutil.rmtree(builddir_path, ignore_errors=True)

def cmake(args=""):
    if not isPathExists(builddir_path):
        os.makedirs(builddir_path)
    if getCurrentDirname() != builddir_name:
        os.chdir(builddir_name+"/")
    cmake_ = subprocess.Popen(["cmake",".."],  stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    output, errors_cmake = cmake_.communicate()
    print(output, errors_cmake)

def make(args=""):
    if not isPathExists(builddir_path):
        print("Project not yet assembled.")
        return
    if getCurrentDirname() != builddir_name:
        os.chdir(builddir_name+"/")
    make_ = subprocess.Popen(["make" if opsystem == "Linux" or opsystem == "Mac" else "nmake -f Makefile.win"],
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    output, errors_make = make_.communicate()
    print(output, errors_make)

def run(args=""):
    if not isPathExists(builddir_path):
        print("Project not yet assembled.")
        return
    if getCurrentDirname() != builddir_name:
        os.chdir(builddir_name+"/")
    run_ = subprocess.Popen(["./"+project_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    output, errors = run_.communicate()
    print(output, errors)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-cmake', action="store", dest='cmake_build',help='Build cmake project.')
    parser.add_argument('-make', action="store", dest='make_build',help='Build make project.')
    parser.add_argument('-run', action="store", dest='run',help='Run compiled program.')
    parser.add_argument('-clear', action="store", dest='clear_build',help='Clear project.')
    parser.add_argument('-full', action="store", dest='full_rebuild',help='Rebuild and run project.')
    
    args = parser.parse_args()

    if args.clear_build:
        clear()
    if args.cmake_build:
        cmake()
    if args.make_build:
        make()
    if args.run:
        run()
    
    if args.full_rebuild:
        clear(); cmake(); make(); run()