#version 140

#extension GL_ARB_explicit_attrib_location : require

layout(location = 0) out vec4 color;

in vec4 vertexColor;
in vec2 texcoord;
in float light;

uniform sampler2D texture1;

void main()
{
    vec4 tex_color = texture(texture1, texcoord);
    color = tex_color*vertexColor*vec4(light, light, light, 1.0);
    
} 