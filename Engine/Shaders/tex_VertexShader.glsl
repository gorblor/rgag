#version 140

#extension GL_ARB_explicit_attrib_location : require

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_texture;
layout(location = 2) in vec4 in_color;
layout(location = 3) in vec3 in_normal;

uniform mat4 model_matrix, view_matrix, projection_matrix;

out vec2 texcoord;
out vec4 vertexColor;

out float light;

void main()
{
    texcoord = in_texture;
    vertexColor = in_color;

    vec3 light_position = vec3(0.0, 0.0, 0.0);
    vec3 eye_position = vec3(0.0, 0.0, 0.0);
 
    int material_shininess = 20;
    float material_kd = 2.5;
    float material_ks = 1.1;

    vec3 world_position = mat3(model_matrix) * in_position;//careful here

    vec3 world_normal = normalize(mat3(model_matrix) * in_normal);

    vec3 L = normalize(light_position - world_position);//light direction
    vec3 V = normalize(eye_position - world_position); //view direction

    //Lambert term
    float LdotN = max(0, dot(L,world_normal));
 
    //consider diffuse light color white(1,1,1)
    //all color channels have the same float value
    float diffuse = material_kd * LdotN;
 
    float specular = 0;

    if(LdotN > 0.0)
    {
        //can use built-in max or saturate function
        vec3 R = -normalize(reflect(L,world_normal));//reflection
        specular = material_ks * pow( max(0, dot( R, V)), material_shininess);
    }

    //pass light to fragment shader
    light = diffuse + specular;
 
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position, 1);

}