#version 140

#extension GL_ARB_explicit_attrib_location : require

in vec4 vertexColor; // Входная переменная из вершинного шейдера (то же название и тот же тип)
layout(location = 0) out vec4 color;

void main()
{
    color = vertexColor;
} 