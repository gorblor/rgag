#version 140

#extension GL_ARB_explicit_attrib_location : require

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec4 in_color;

uniform mat4 model_matrix, projection_matrix, view_matrix;

out vec4 vertexColor;

void main()
{
    vertexColor = in_color;
    
    gl_Position = projection_matrix*view_matrix*model_matrix*vec4(in_position,1);
}