
#pragma once

#include <iostream>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include "EngineInfo.h"
#include "EngineListenger.h"
#include <Util/EngineDebug.h>
#include <Managers/SceneManager.h>

namespace ENGINE
{
    namespace CORE
    {
        namespace INIT
        {
            class Init_GLEW
            {
            public:
                static bool Init();
            private:
                static bool glewExperimental;
            };

            class Init_GLUT
            {
            public:
                static void init(int argc, char* argv[], const ENGINE::CORE::WindowInfo& windowInfo, const ENGINE::CORE::ContextInfo& contextInfo, const ENGINE::CORE::FrameBufferInfo& framebufferInfo);
                static void run();
                static void stop();

                void Fullscreen();
                static void OpenGLInfo(const ENGINE::CORE::WindowInfo& windowInfo, const ENGINE::CORE::ContextInfo& contextInfo);
            private:
                static void idleCallback(void);
                static void displayCallback(void);
                static void reshapeCallback(int width, int height);
                static void closeCallback();
            
            private:
                static ENGINE::CORE::EngineListenger* listener;
                static ENGINE::CORE::WindowInfo windowInformation;
    
            public:
                static void setListener(MANAGERS::SceneManager*& iListener);
        
            };
        }
    }
}