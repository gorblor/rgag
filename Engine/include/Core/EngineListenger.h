#pragma once

namespace ENGINE
{
    namespace CORE
    {
        class EngineListenger
        {
        public:
            virtual ~EngineListenger() = 0;
            virtual void notifyBeginFrame() = 0;
            virtual void notifyDisplayFrame() = 0;
            virtual void notifyEndFrame() = 0;
            virtual void notifyReshape(int width, int height, int previous_width, int previous_height) = 0;
        };

        inline EngineListenger::~EngineListenger(){
        //implementation of pure virtual destructor
        }  
    }
}