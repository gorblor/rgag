
#pragma once
#include <string>

#include <GL/glew.h>
#include <GL/freeglut.h>

namespace ENGINE
{
    namespace CORE
    {
        enum API
        {
            OPENGL,
            DIRECT3D,
            VULKAN
        };

        struct WindowInfo
        {
            std::string w_name;
            int argc; char** argv;
            int width, height;
            int position_x, position_y;
            bool isReshapable;
            bool isFullscreen;

            WindowInfo()
            {
                w_name = "ENGINE";
                width = 800; height = 600;
                position_x = 0; position_y = 0;
                isReshapable = true;
                isFullscreen = false;
            }

            WindowInfo(std::string w_name, int width, int height, int position_x = 0, int position_y = 0, bool isReshapable=false, bool isFullscreen=false)
            {
                this->w_name = w_name;
                this->argc = argc; this->argv = argv;
                this->width = width; this->height = height;
                this->position_x = position_x; this->position_y = position_y;
                this->isReshapable = isReshapable;
                this->isFullscreen = isFullscreen;
            }

            WindowInfo(const WindowInfo& windowInfo)
            {
                this->w_name = windowInfo.w_name;
                this->argc = windowInfo.argc; this->argv = windowInfo.argv;
                this->width = windowInfo.width; this->height = windowInfo.height;
                this->position_x = windowInfo.position_x; this->position_y = windowInfo.position_y;
                this->isReshapable = windowInfo.isReshapable;
                this->isFullscreen = windowInfo.isFullscreen;
            }

            void operator=(const WindowInfo& windowInfo)
            {
                this->w_name = windowInfo.w_name;
                this->argc = windowInfo.argc; this->argv = windowInfo.argv;
                this->width = windowInfo.width; this->height = windowInfo.height;
                this->position_x = windowInfo.position_x; this->position_y = windowInfo.position_y;
                this->isReshapable = windowInfo.isReshapable;
                this->isFullscreen = windowInfo.isFullscreen;
            }
        };

        struct ContextInfo
        {
            int version_major, version_minor;
            API api;
            bool core;

            ContextInfo()
            {
                version_major = 1; version_minor = 0;
                api = API::OPENGL;
                core = true;
            }

            ContextInfo(int version_major, int version_minor, API api=API::OPENGL, bool core=true)
            {
                this->version_major = version_major; this->version_minor = version_minor;
                this->api = api;
                this->core = core;
            }

            ContextInfo(const ContextInfo& contextInfo)
            {
                this->version_major = contextInfo.version_major;
                this->version_minor = contextInfo.version_minor;
                this->api = contextInfo.api;
                this->core = contextInfo.core;
            }

            void operator=(const ContextInfo& contextInfo)
            {
                this->version_major = contextInfo.version_major;
                this->version_minor = contextInfo.version_minor;
                this->api = contextInfo.api;
                this->core = contextInfo.core;
            }
        };

        struct FrameBufferInfo
        {
            unsigned int flags;
            bool msaa;

            FrameBufferInfo()
            {
                flags = GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH;
                msaa = false;
            }

            FrameBufferInfo(bool color, bool depth, bool stencil, bool msaa=false)
            {
                flags = GLUT_DOUBLE;
                if (color)
                    flags |= GLUT_RGBA | GLUT_ALPHA;
                if (depth)
                    flags |= GLUT_DEPTH;
                if (stencil)
                    flags |= GLUT_STENCIL;
                if (msaa)
                    flags |= GLUT_MULTISAMPLE;
                this->msaa = msaa;
            }

            FrameBufferInfo(const FrameBufferInfo& fbi)
            {
                this->flags = fbi.flags;
                this->msaa = fbi.msaa;
            }

            void operator=(const FrameBufferInfo& fbi)
            {
                this->flags = fbi.flags;
                this->msaa = fbi.msaa;
            }
        };
    }
}