#pragma once
#include "Core/EngineInit.h"
#include "Managers/SceneManager.h"
#include "Managers/MaterialManager.h"

namespace ENGINE
{
    class Engine
    {
    public:
        Engine();
        ~Engine();
 
        //OpenGL and manager init
        bool Init(int argc, char* argv[]);
        bool Init(int argc, char* argv[], ENGINE::CORE::WindowInfo window, ENGINE::CORE::ContextInfo context, ENGINE::CORE::FrameBufferInfo frameBufferInfo);
 
        //Loop
        void Run();
 
        //Getters
        MANAGERS::SceneManager*  GetScene_Manager()  const;
        MANAGERS::MaterialManager* GetMaterial_Manager() const;
        MANAGERS::ObjectManager* GetObject_Manager() const;
 
    private:
        MANAGERS::SceneManager*  m_scene_manager;
        MANAGERS::MaterialManager* m_material_manager;
        MANAGERS::ObjectManager* m_object_manager; 
    };
}