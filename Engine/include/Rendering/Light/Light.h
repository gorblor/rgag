#pragma once

#include "../GameObject.h"
#include <glm/glm.hpp>

namespace ENGINE
{
    namespace RENDERING
    {
        namespace LIGHT
        {
            class Light : public GameObject
            {
            public:
                Light();
                virtual ~Light();

                virtual void Draw() override;
                virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix) override;
                virtual void Update() override;
                virtual void Destroy() override;
        
                virtual GLuint GetVao() const override;
                virtual const std::vector<GLuint>& GetVbos() const override;

                // virtual const Material GetMaterial() const override;
				// virtual void SetMaterial(Material material) override;
                
            private:
                GLuint vao;
                std::vector<GLuint> vbos;
                glm::vec4 m_diffuse, m_ambient, m_specular, m_position;
                float m_attenuration;
            };
        }
    }
}