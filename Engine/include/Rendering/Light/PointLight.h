#pragma once

#include "Light.h"
#include <glm/glm.hpp>

namespace ENGINE
{
    namespace RENDERING
    {
        namespace LIGHT
        {
            class PointLight : public Light
            {
            public:
                PointLight();
                ~PointLight();

                void Create();
                virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix) override final;
                virtual void Update() override final;
            };
        }
    }
}