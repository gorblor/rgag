#pragma once

#include "Light.h"
#include <glm/glm.hpp>

namespace ENGINE
{
    namespace RENDERING
    {
        namespace LIGHT
        {
            class SpotLight : public Light
            {
            public:
                SpotLight();
                ~SpotLight();

                void Create();
                virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix) override final;
                virtual void Update() override final;
            };
        }
    }
}