#pragma once

#include <vector>
#include <iostream>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include "VertexFormat.h"
#include "Texture/Material.h"

namespace ENGINE
{
    namespace RENDERING
    {
        class GameObject
        {
            public:
            virtual ~GameObject() = 0;

            virtual void Draw() = 0;
            virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix) = 0;
            virtual void Update()= 0;
            virtual void Destroy() = 0;
        
            virtual GLuint GetVao() const = 0;
            virtual const std::vector<GLuint>& GetVbos() const = 0;

            virtual void SetMaterial(Material material) = 0;
            virtual const Material GetMaterial() const = 0;
        };
        
        inline GameObject::~GameObject()
        {//blank
        }
    }
}