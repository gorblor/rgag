#pragma once

#include "Object.h"

namespace ENGINE
{
    namespace RENDERING
    {
        namespace OBJECTS
        {
            class Quad : public Object
            {
            public:
                Quad();
                ~Quad();
            
                void Create();
                virtual void Draw() override final;
                virtual void Update() override final;
            };
        }
    }
}