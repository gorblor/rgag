#pragma once

#include <vector>
#include <map>
#include "../GameObject.h"

namespace ENGINE
{
    namespace RENDERING
    {
        namespace OBJECTS
        {
            class Object : public GameObject
            {
            public:
                Object();
                virtual ~Object();
                // methods from interface
                virtual void Draw() override;
                virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix) override;
                virtual void Update() override;
                virtual void Destroy() override;
        
                virtual GLuint GetVao() const override;
                virtual const std::vector<GLuint>& GetVbos() const override;

                virtual const Material GetMaterial() const override;
				virtual void SetMaterial(Material material) override;
    
            protected:
                GLuint vao;
                std::vector<GLuint> vbos;
                Material material;
            };
        }
    }
}