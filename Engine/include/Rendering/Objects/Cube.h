#pragma once

#include "Object.h"

namespace ENGINE
{
    namespace RENDERING
    {
        namespace OBJECTS
        {
            class Cube : public Object
            {
            public:
                Cube();
                Cube(glm::vec3 position, glm::vec3 rotation=glm::vec3(1.f,0.f,0.f), glm::vec3 scale=glm::vec3(1.f,1.f,1.f));
                ~Cube();
            
                void Create();
                virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix) override final;
                virtual void Update() override final;
    
            private:
                glm::mat4 model_matrix;
                time_t timer;
            };
        }
    }
}