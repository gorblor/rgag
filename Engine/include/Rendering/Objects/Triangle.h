#pragma once

#include "Object.h"

namespace ENGINE
{
    namespace RENDERING
    {
        namespace OBJECTS
        {
            class Triangle : public Object
            {
            public:
                Triangle();
                ~Triangle();
        
                void Create();
                virtual void Update() override final;
                virtual void Draw() override final;
            };
        }
    }
}