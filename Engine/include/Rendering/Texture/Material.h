#pragma once

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>

#include <vector>
#include <map>


namespace ENGINE
{
    namespace RENDERING
    {
        struct Texture
        {
            std::string tex_name;
            std::string tex_path;
            std::string tex_type;
            unsigned int width, height;
        };

        class Material
        {
        public:
            Material();
            ~Material();

            void SetTexture(std::string textureName, GLuint texture);
            GLuint GetTexture(std::string textureName);

            void SetProgram(GLuint shaderName);
            GLuint GetProgram();

            inline bool isEmpty() { return false; }
        private:
            glm::vec4 mat_ambient, mat_diffuse, mat_specular;
            float shine;
            GLuint program;
            std::map<std::string, GLuint> textures;
        };
    }
}