#pragma once
#include <glm/glm.hpp>

namespace ENGINE
{
    namespace RENDERING
    {
        struct VertexFormat
        {
            glm::vec3 position;
            glm::vec2 texture;
            glm::vec4 color;
            glm::vec3 normal;

            VertexFormat(const glm::vec3 &iPos, const glm::vec2 &iTexture, const glm::vec4 &iColor=glm::vec4(1.0, 1.0, 1.0, 1.0), const glm::vec3 &iNormal=glm::vec3(1.0, 1.0, 1.0))
            {
                position = iPos;
                texture = iTexture;
                color = iColor;
                normal = iNormal;
            }
    
            VertexFormat() {}
        };
        
    }
}