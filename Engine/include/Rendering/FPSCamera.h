#pragma once

#include <glm/glm.hpp>

namespace ENGINE
{
    namespace RENDERING
    {
        namespace CAMERA
        {
            class FPSCamera
            {
            public:
                FPSCamera();
                ~FPSCamera();

                void UpdateView();
                void KeyPressed(const unsigned char key, int x, int y);
                void MouseMove(int x, int y);
                void MousePressed(int button, int state, int x, int y);

                glm::mat4 GetViewMatrix() const;
                glm::vec3 GetEyePosition() const;
            private:
                glm::vec2 mousePosition;
                glm::vec3 eye;
                glm::mat4 viewMatrix;
                float roll, pitch, yaw;
                bool isMousePressed;
            };
        }
    }
}