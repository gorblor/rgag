#pragma once

#include "../Rendering/Texture/Material.h"
#include "../Rendering/Texture/TextureLoader.h"
#include "ShaderManager.h"

namespace ENGINE
{
	namespace MANAGERS
	{
        class MaterialManager
        {
        public:
            MaterialManager(void);
            ~MaterialManager(void);


            void Init();
            void CreateMaterial(std::string materialName, std::vector<ENGINE::RENDERING::Texture> textures);
            void CreateMaterial(std::string materialName, ENGINE::RENDERING::Texture texture);
            const ENGINE::RENDERING::Material GetMaterial(std::string materialName);

            RENDERING::TextureLoader* GetTexture_Loader() const;

        private:
            MANAGERS::ShaderManager* m_shader_manager;
            std::map<std::string, ENGINE::RENDERING::Material> materials;

            RENDERING::TextureLoader* m_texture_loader;
        };
    }
}