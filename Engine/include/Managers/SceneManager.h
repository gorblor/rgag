#pragma once

#include "ObjectManager.h"
#include "../Core/EngineListenger.h"
#include "../Rendering/FPSCamera.h"

namespace ENGINE
{
    namespace MANAGERS
    {
        class SceneManager : public ENGINE::CORE::EngineListenger
        {
        public:
            SceneManager();
            ~SceneManager();
        
            virtual void notifyBeginFrame();
            virtual void notifyDisplayFrame();
            virtual void notifyEndFrame();
            virtual void notifyReshape(int width, int height, int previous_width, int previous_height);

            ENGINE::RENDERING::CAMERA::FPSCamera* GetCamera() const;

            void SetObjectManager(MANAGERS::ObjectManager*& object_manager);
        private:
            ENGINE::MANAGERS::ObjectManager* object_manager;
            glm::mat4 projection_matrix, view_matrix;
            ENGINE::RENDERING::CAMERA::FPSCamera* camera;
        };
    }
}