#pragma once

#include <unordered_map>

#include "../Rendering/GameObject.h"
 
namespace ENGINE
{
    namespace MANAGERS
    {
        class ObjectManager
        {
            public:
                ObjectManager();
                ~ObjectManager();
        
                void Draw();
                void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix);
                void Update();

                void DeleteObject(const std::string& gameObjectName);
                const RENDERING::GameObject& GetObject(const std::string& gameObjectName) const;

                void DeleteObject_NDC(const std::string& gameObjectName);
                const RENDERING::GameObject& GetObject_NDC(const std::string& gameObjectName) const;

                void SetObject(const std::string& gameObjectName, RENDERING::GameObject* gameObject);
        
            private:
                std::map<std::string, RENDERING::GameObject*> gameObjectList;
                std::map<std::string, RENDERING::GameObject*> gameObjectList_NDC;
        };
    }
}