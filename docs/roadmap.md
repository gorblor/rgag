# Roadmap of our provect

Description of next releases

Текущая версия: *0.0.0.0*

**Alpha**

*ver 0.0.0.0*
```diff
front-end:
+ Add python file for build and clear project with simple console interface

back-end:
+ Adjust cmake for work with project

tests:
- "No in this version"
```

*ver 0.0.0.1*
```diff
front-end:
- Add test functions to python file

back-end:
+ realise opengl backend for create window
+ realise simple drawing system (framefabric)

tests:
- realise window creation test
- realise drawing test
```

*ver 0.0.0.2*
```diff
front-end:
- "Not for this version"

back-end:
- realise texture mapping
- realise debugging system
- realise cmake lib config

tests:
- ????????????????
```